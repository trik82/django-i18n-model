""" Tests three different ways of referring to models
"""

from .models import *
from .model_test_case import ModelTestCase


class ModelReferenceTestCase(ModelTestCase):
    def test_reference_by_class_name(self):
        """
        Test that using the original name + 'I18N' suffix works
        """
        self.assertForeignKey(PostI18N, 'i18n_source', Post)

    def test_reference_by_class_meta(self):
        """
        Test that using the class object in Meta options works
        """
        self.assertForeignKey(BookTranslation, 'i18n_source', Book)

    def test_reference_by_string_name_meta(self):
        """
        Test that referring to class name in string format in Meta works
        """
        self.assertForeignKey(ArticleTranslation, 'i18n_source', Article)

    def test_reference_by_model_name_meta(self):
        """
        Test that referring to model name in string format in Meta works
        """
        self.assertForeignKey(NewsTranslation, 'i18n_source', News)
