""" Tests for unique field handling
"""

from django.conf import settings
from django.db import models

from .models import ModelWithUniqueField, ModelWithUniqueFieldI18N
from .model_test_case import ModelTestCase


class UniqueFieldTestCase(ModelTestCase):
    """
    Tests ensure that unique fields are unique per langauge
    """

    def test_unique_field(self):
        """
        Unique field in translation model should no longer be unique
        """
        self.assertHasField(ModelWithUniqueFieldI18N, 'slug')
        self.assertFalse(ModelWithUniqueFieldI18N._meta.fields[2]._unique)

    def test_unique_together(self):
        """
        Unique fields from original model should now be unique_together
        """
        self.assertTrue(('i18n_language', 'slug') in
                        ModelWithUniqueFieldI18N._meta.unique_together)

