from django import forms
from django.utils.translation import get_language
from django.utils.encoding import smart_text
from django.db import models
from django.core.exceptions import ObjectDoesNotExist


class I18NModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        lang = get_language()
        translation = obj.translations.get_by_lang(lang)
        try:
            for field in translation._meta.fields:
                if type(field) in [
                    models.TextField,
                    models.CharField,
                    models.SlugField
                ] and field.name != 'i18n_language':
                    setattr(obj, field.name, getattr(translation, field.name))
        except ObjectDoesNotExist:
            pass
        return smart_text(obj)